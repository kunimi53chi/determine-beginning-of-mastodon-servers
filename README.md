# Mastodonのサーバー開始時期を割り出す

- 最古のアカウントのcreated_atがv2.4.0より前に作られたのか？
- account_id が 1 であるアカウントの created_at を調べる
- ドメイン/web/accounts/1 でリダイレクトした先のユーザーのアドレスに?min_id=1をつけて再度取得すると、インスタンス最古の投稿が取れるかも。
- インスタンスの一覧をパッと取るなら、末代くらいの規模のサーバーの/api/v1/instance/peersを見るのがよい

（アイデア協力：まおーさん、おささん、やきたまさん、Cutls Pさん）

- https://docs.toot.app/api/rest/accounts/
- https://docs.toot.app/api/entities/#account
- https://fediverse.network/
- https://hakabahitoyo.wordpress.com/2018/01/15/peers-api/

Accountsから取ってきたほうがよい。
Instancesからは直接的な情報は得られないが、Instances/Peersから一覧取得は可能。

- mstdn.maud.io, mstdn.beerなどから、連合サーバーの取得。
- GET /api/v1/accounts/:id　で、idを1からインクリメントしていって、引っ掛かり次第、それを取得。10以上は無理。やだ。
- 取得したAccountのcreated_atが2018/05/23以前であれば、v2.4.0より前に稼働していたこととする。
- short_descriptionキーがないなら少なくともMastodon v3.0.0未満というのがわかる。

なお、アクティブがどれほどかというところは考慮しないといけないが、定量的指標は定まっていない。

## 詳細設計

Pythonで書く。 requestsとdatasetでお手軽なやつにする。

### これはええねん

REST APIをPythonからたたいて、永続化をとりあえずやりつつ、入稿前までUpdateしようかというところ

- https://2.python-requests.org//en/latest/

### これはやらん

- PythonでDB周りまでやってないけど、でもやれるようになったほうがいいよねー。Docker作ってDBコンテナ立ててそこで保存って感じだよねー
- https://github.com/halcy/Mastodon.py

Mastodon.pyはメンテが続いてるから、いっちょこれ使ってやるか、と思ったら認証が必要なので、やることに対して重い
