import api
import csvwriting
import datetime
import dateutil.parser
from pprint import pprint

# Instances/Peerから一覧を取得する。ドメインは個人的なチョイスです
peer_domains = [
    "mstdn-workers.com",
    "mstdn.maud.io",
    "mstdn.kemono-friends.info",
    "mstdn.beer",
    "oransns.com",
    "vocalodon.net",
    "mastodon.juggler.jp",
    "kancolle.social"
    # === test server ===
    # "mastodon.love"
    # "bombayguy.in"
]

def handle():

    peers = []
    for domain in peer_domains:
        get_response_of_peers = api.get_peers(domain)
        if is_succeed(get_response_of_peers):
            peers.append(get_response_of_peers)

    if not peers:
        print("no peers!?")
        exit()
    
    flat_peers = [x for row in peers for x in row]
    unique_peers = list(set(flat_peers))
    
    my_recommending_servers = []
    for server in unique_peers:
        pprint(server + ' is processing...')
        instance = api.get_instance(server) # dict
        # contact_account キーがあったらmastodon or gab
        if 'contact_account' not in instance:
            continue
        # short_descriptionキーがあったらv2.9.2以上
        if 'short_description' not in instance:
            continue

        account_id = 1
        earliest_account =  {}
        for retry in range(10): # idが10以上から始まってたら泣いてしまいます
            earliest_account = api.get_earliest_account(server, account_id)
            if is_succeed(earliest_account):
                break
        
        if 'created_at' not in earliest_account:
            print('とれんかったわごめん')
            continue
        if not _is_datetime_earlier(earliest_account['created_at']):
            print('おしいわ')
            continue

        combined = {
            'title': instance['title'],
            'uri': instance['uri'],
            'version': instance['version'],
            'languages': instance['languages'],
            'registrations': instance['registrations'],
            'acct': earliest_account['acct'],
            'locked': earliest_account['locked'],
            'username': earliest_account['username'],
            'display_name': earliest_account['display_name'],
            'url': earliest_account['url'],
            'created_at': earliest_account['created_at']
        }
        my_recommending_servers.append(combined)

    csvwriting.dict_writer(my_recommending_servers)
    return "ALL IS DONE!"


def is_succeed(response) -> bool:
    return response != "error occured" and response != "404" and response != "bad response" and response != "timeout"


def _is_datetime_earlier(created_at) -> bool:
    """
    v2.4.0リリースよりcreated_atが古いことを判定する
    """

    target = dateutil.parser.parse('2018-05-23T03:18:00.000Z') # JST
    JST = datetime.timezone(datetime.timedelta(hours=+9), 'JST')
    jst_created_at = dateutil.parser.parse(created_at).astimezone(JST)

    return jst_created_at < target


if __name__ == "__main__":
    pprint(handle())
