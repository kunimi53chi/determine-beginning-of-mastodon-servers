import sys
import requests
import json
from pprint import pprint

def get_earliest_account(domain, account_id):
    url = 'https://' + domain + '/api/v1/accounts/' + str(account_id)
    try:
        r = requests.get(url, timeout=2.0)
    except Exception as e:
        # raise e
        return "error occured"
    
    if r.status_code == 404:
        return "404"
    
    try:
        if r.status_code == 200 and isJsonFormat(r.text):
            return r.json()
    except Exception as e:
        return "bad response"
    
    return "bad response"    


def get_peers(domain):
    url = 'https://' + domain + '/api/v1/instance/peers'
    # print(url)
    try:
        r = requests.get(url, timeout=2.0) 
    except Exception as e:
        # raise e
        return "error occured"
    
    if r.status_code == 404:
        print("404")
        return "404"

    if r.status_code == 200:
        return json.loads(r.text)

    print("bad response")
    return "bad response"

def get_instance(domain):
    url = 'https://' + domain + '/api/v1/instance'
    # print(url)
    try:
        r = requests.get(url, timeout=2.0) 
    except Exception as e:
        # raise e
        return "error occured"
    
    if r.status_code == 404:
        print("404")
        return "404"

    try:
        if r.status_code == 200 and isJsonFormat(r.text):
            return r.json()
    except Exception as e:
        return "bad response"
    
    return "bad response"


def isJsonFormat(line):
    try:
        json.loads(line)
    except json.JSONDecodeError as e:
        print(sys.exc_info())
        print(e)
        return False

    return True

def sandbox():
    return ""

if __name__ == '__main__':
    pprint(get_instance('therealblue.de'))
