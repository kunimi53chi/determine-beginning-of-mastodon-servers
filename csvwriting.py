# https://docs.python.org/ja/3/library/csv.html
import csv

def dict_writer(my_recommending_servers: list):
    """
    辞書が入ってるリストをCSV出力するよ
    """
    with open('servers.csv', 'w') as s:
        header = ['title','uri','version','languages','registrations','acct','locked','username','display_name','url','created_at']
        writer = csv.DictWriter(s, header)
        writer.writeheader()
        writer.writerows(my_recommending_servers)

if __name__ == "__main__":
    dict_writer([{"a": "b"}])
